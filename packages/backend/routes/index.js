const router = require('express').Router();
const userRouter = require("./user");
const metaRouter = require('./meta');

router.get("/status", (_, res) => res.status(200).json({status: "OK"}));

router.use('/user', userRouter);
router.use('/meta', metaRouter);

module.exports = router;