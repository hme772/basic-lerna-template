const express = require('express');
const {getAllUserTypes} = require("../controls/meta");
const validateGetAllUserAndAppointmentTypes = require("../validations/meta")
const validateResource = require('../middlewares/validateResource');

const router = express.Router();
router.get("/", validateResource(validateGetAllUserAndAppointmentTypes), getAllUserTypes);

module.exports = router;