const config = require("../config/config");
const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(config.sendGrid.apiKey);


const sendEmail = async ({message,email}) => {
    const msg1 = {
        to: email,
        from: config.sendGrid.fromEmail,
        subject: config.sendGrid.subject,
        text: `Your Message: ${message}`,
        html: `<h1>Your Message:</h1><br/><b> ${message}</b><br><p>This message is relevant for up to 5 minutes</p>`,
    };

    await sgMail.send(msg1);
}
module.exports = {sendEmail};
