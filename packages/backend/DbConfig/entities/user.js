
const getUserTypeEntity = () => `id integer NOT NULL,
            type text NOT NULL,
            CONSTRAINT user_type_pkey PRIMARY KEY (id)`;

const getUserEntity = (schema) => `id INTEGER NOT NULL,
            first_name text,
            last_name text,
            phone_number text,
            email text,
            user_type integer NOT NULL,
            "creation_time" integer NOT NULL,
            update_time integer NOT NULL,
            is_active boolean NOT NULL DEFAULT true,
            CONSTRAINT users_pkey PRIMARY KEY (id),
            CONSTRAINT id_check CHECK (id >= 100000000 AND id <= 999999999),
            FOREIGN KEY (user_type) REFERENCES ${schema}."user_type"(id)`;

module.exports = {getUserTypeEntity, getUserEntity};