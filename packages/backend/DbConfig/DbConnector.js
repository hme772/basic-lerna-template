const config = require("../config/config");
const globals = require("../config/globals");

const { Client } = require("pg");
const initDb = require("./schema");


//for DB to check that everything works
const checkDbConnection = async () => {
  const client = globals.pg.client;
  try {
    const { rows } = await client.query("SELECT current_user");
    console.log(rows[0]["current_user"]);
  } catch (err) {
    console.error(err);
  } finally {
    client.release();
  }
};

async function connectPostgres() {
  try{
    const client = new Client({ ...config.postgres.connect });
    await client.connect();
    await initDb(client, "template");

    globals.pg.client = client;
    client.on('error',async err => {
      globals.pg.client= undefined;
      console.error(`Postgres disconnected with error.\nwill attempt to reconnect in ${config.postgres.reconnectTime} seconds.\nerror:`, err.stack);
      await new Promise(r => setTimeout(r, config.postgres.reconnectTime * 1000));
      await connectPostgres();
    });
    console.log(`postgres connected to http://${ config.postgres.connect.host }:${ config.postgres.connect.port }/${ config.postgres.connect.database }`);
  }catch(e){
    console.log(`postgres failed to connect trying again in ${config.postgres.reconnectTime} seconds.\nerror:`,e);
    await new Promise(r => setTimeout(r, config.postgres.reconnectTime * 1000));
    await connectPostgres();
  }

}

async function disconnectPostgres() {
  await globals.pg.client.end();
  console.log(`Closing postgres on ${ config.postgres.connect.host }:${ config.postgres.connect.port }/${ config.postgres.connect.database }`);
}

module.exports = {connectPostgres, disconnectPostgres};
