const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");
const morgan = require("morgan");
const routers = require('./routes/index');
const errorHandler = require('./middlewares/errorHandler');

const app = express();

// use swagger
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./config/swagger/main.yaml');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//for parsing data
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({limit: "1mb"})); // 100kb default
app.use(cors({origin: "*"}));
app.use(helmet());
app.use(morgan("combined"));

app.use(routers);
app.get("/", (req, res) => {
    res.status(200).send("hello from server!");
});

app.use(errorHandler);

module.exports = app;