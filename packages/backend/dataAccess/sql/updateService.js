const getUpdateQueryAndParams = (req, table) => {
    const schema = req.body.schema || 'template';
    delete req.body.schema;
    let query=`UPDATE ${schema}.${table} SET \n`,index=1;

    const keysToUpdate = Object.keys(req.body);
    const params = [...Object.values(req.body), req.params.id];

    let queryContent = keysToUpdate.map((key)=>{
        return `${key} = $${index++},`
    })
    query += queryContent.join('\n');
    query = query.slice(0,-1);
    query += `\nWHERE id = $${index} \nRETURNING *;`;
    console.log(query, params)
    return {query, params};
};

module.exports = {getUpdateQueryAndParams};