const getUserTypesQuery = ({schema}) => `SELECT * FROM ${schema || 'template'}.user_type;`;

module.exports = {getUserTypesQuery};
