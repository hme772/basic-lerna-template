const {getUpdateQueryAndParams} = require("./updateService");

const getUsersQuery = ({schema}) => `SELECT * FROM ${schema || 'template'}.users WHERE is_active=true`;
const getUserByEmailQuery = ({schema}) => `${getUsersQuery({schema})} AND email=$1;`;
const getIsUserExistQuery = ({schema}) => `${getUsersQuery({schema})} AND email=$1 OR id=$2;`;
const getUserByIdQuery = ({schema}) => `${getUsersQuery({schema})} AND id=$1;`;
const getAddUserQuery = ({schema}) => `INSERT INTO 
        ${schema || 'template'}.users (creation_time, update_time, id, first_name, last_name, phone_number, email, user_type )
        VALUES
        ($1, $2, $3, $4, $5, $6, $7, $8) \nRETURNING *;`;
const getUpdateUserQueryAndParams = (req) => {
        return getUpdateQueryAndParams(req,'users')
};
module.exports = {getUsersQuery, getUserByEmailQuery, getIsUserExistQuery, getUserByIdQuery, getAddUserQuery, getUpdateUserQueryAndParams};