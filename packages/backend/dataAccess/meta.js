const {getUserTypesQuery} = require('./sql/meta');
const globals = require("../config/globals");

const getAllUserTypesHandler = async ({schema}) => {
    const userTypesQuery = getUserTypesQuery({schema});
    const userTypesResult = await globals.pg.client.query(userTypesQuery);
    return {userTypes: userTypesResult?.rows || []};
};

module.exports = {getAllUserTypesHandler};