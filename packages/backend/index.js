const config = require('./config/config');
const {connectPostgres} = require("./DbConfig/DbConnector");
const app = require('./server');

app.listen(config.server.port, async (error) => {
    if (!error) {
        try {
            await connectPostgres();
        } catch (err) {
            console.error(err);
        }
        console.log(
            "Server is Successfully Running, and App is listening on port " + config.server.port
        );
    } else {
        console.log("Error occurred, server can't start", error);
    }
});