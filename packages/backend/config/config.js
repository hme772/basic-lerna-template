const dotenv = require('dotenv');
dotenv.config();

module.exports = {
	server: {
		port: process.env.SERVER_PORT || 3000,
	},
	postgres: {
		isConnect: process.env.POSTGRES_CONNECT ? process.env.POSTGRES_CONNECT.trim().toLowerCase() === 'true' : true,
		connect: {
			user: process.env.POSTGRES_USER || 'postgres',
			host: process.env.POSTGRES_HOST || 'localhost',
			database: process.env.POSTGRES_DATABASE || 'postgres',
			password: process.env.POSTGRES_PASSWORD || 'postgres',
			port: process.env.POSTGRES_PORT ? parseInt(process.env.POSTGRES_PORT) : 5432
		},
		reconnectTime: 15 //seconds
	},
	sendGrid: {
		apiKey: process.env.SENDGRID_API_KEY || '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',
		fromEmail: process.env.SENDGRID_EMAIL ||'otp-project@walla.co.il',
		subject: process.env.SENDGRID_SUBJECT || "Otp Code",
	},

};