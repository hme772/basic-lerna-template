const Joi = require('joi');
const {validationFunctionsObj} = require('./validationService');

const validateGetIsUserExist = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
    }),
    params: validationFunctionsObj.empty,
    query: Joi.object({
        id: validationFunctionsObj.id,
        email: validationFunctionsObj.email,
    }),
});

const validateGetAllUsers = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
    }),
    params: validationFunctionsObj.empty,
    query: Joi.object({
        email: validationFunctionsObj.optional_email.optional(),
    }),
});

const validateGetUserById = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
    }),
    params: Joi.object({
        userId: validationFunctionsObj.id,
    }),
    query: validationFunctionsObj.empty,
});

const validateAddUser = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
        id: validationFunctionsObj.id,
        firstName: validationFunctionsObj.name,
        lastName: validationFunctionsObj.name,
        phone: validationFunctionsObj.phone,
        email: validationFunctionsObj.email,
        userType : validationFunctionsObj.userType,
    }),
    params: validationFunctionsObj.empty,
    query: validationFunctionsObj.empty,
});

const validateUpdateUser = Joi.object({
    body: Joi.object({
        schema: validationFunctionsObj.schema,
        first_name: validationFunctionsObj.optional_name,
        last_name: validationFunctionsObj.optional_name,
        phone_number: validationFunctionsObj.optional_phone,
        email: validationFunctionsObj.optional_email,
        user_type : validationFunctionsObj.optional_userType,
        is_active: validationFunctionsObj.optional_is_active,
    }).custom((value, helpers) => {
        if (Object.keys(value).length === 0) {
            return helpers.error('object.empty');
        }
        return value;
    }),
    params: Joi.object({
        id: validationFunctionsObj.id,
    }),
    query: validationFunctionsObj.empty,
});

module.exports = {
    validateGetIsUserExist,
    validateGetAllUsers,
    validateGetUserById,
    validateAddUser,
    validateUpdateUser,
};