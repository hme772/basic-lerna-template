const {validateSchemaOnly} = require('./validationService');

const validateGetAllUserAndAppointmentTypes = validateSchemaOnly;

module.exports = validateGetAllUserAndAppointmentTypes;