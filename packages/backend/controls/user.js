const {getAllUsersHandler, getUserByEmailHandler, getUserByIdHandler, addUserHandler, getIsUserExistHandler,
    updateUserHandler
} = require('../dataAccess/user');

const getIsUserExist = async (req, res) => {
    try {
        const result = await getIsUserExistHandler({schema: req.body.schema, id: req.query.id, email: req.query.email});
        return res.json(result);
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};


const getUserByEmail = async (req, res) => {
    try {
        const result = await getUserByEmailHandler({schema: req.body.schema, email: req.query.email});
        return res.json(result);
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};

const getAllUsers = async (req, res) => {
    try {
        if (req.query.email) return await getUserByEmail(req, res);
        else {
            const result = await getAllUsersHandler({schema: req.body.schema});
            return res.json(result);
        }
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }

};

const getUserById = async (req, res) => {
    try {
        const result = await getUserByIdHandler({schema: req.body.schema, userId: req.params.userId});
        return res.json(result);
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};

const addUser = async (req, res) => {
    try {
        const {schema, id, firstName, lastName, phone, email, userType} = req.body;
        const result = await addUserHandler({schema, id, firstName, lastName, phone, email, userType});
        return res.json({message: `successfully added ${result.rowCount}/1 users`, addedUser: result.addedUser});
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};

const updateUser = async(req, res) => {
    try {
        req.body.update_time = Math.floor(Date.now() / 1000);
        const result = await updateUserHandler(req);
        return res.json({message: `successfully updated ${result.rowCount}/1 users`,updatedUser: result.updatedUser});
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }
};

module.exports = {getAllUsers, getUserByEmail, getUserById, addUser, getIsUserExist, updateUser};