const {getAllUserTypesHandler} = require("../dataAccess/meta");

const getAllUserTypes = async (req, res) => {
    try {
        const {schema} = req.body;
        const result = await getAllUserTypesHandler({schema});
        return res.json(result);
    } catch (err) {
        console.log(err);
        res.status(err.status || 500).send(err);
    }

};

module.exports = {getAllUserTypes};