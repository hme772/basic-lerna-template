const request = require('supertest');
const express = require('express');
const auth = require('../middlewares/auth');
const validateResource = require('../middlewares/validateResource');
const {
    getAllUsers,
    addUser,
    getUserById,
    updateUser,
} = require('../controls/user');

const {
    validateGetAllUsers,
    validateAddUser,
    validateGetUserById,
    validateUpdateUser,
} = require('../validations/user');

const app = express();
app.use(express.json());

jest.mock('../middlewares/auth');
jest.mock('../middlewares/validateResource');
jest.mock('../controls/user');
jest.mock('../validations/user');

// Sample implementations for middleware and controllers to use in tests
auth.protect = jest.fn((req, res, next) => next());
validateResource.mockImplementation((validation) => (req, res, next) => next());

getAllUsers.mockImplementation((req, res) => res.status(200).json([
    { id: '1', email: 'user1@example.com' },
    { id: '2', email: 'user2@example.com' },
]));
addUser.mockImplementation((req, res) => res.status(201).json({ id: '3', email: 'newuser@example.com' }));
getUserById.mockImplementation((req, res) => res.status(200).json({ id: req.params.userId, email: 'user@example.com' }));
updateUser.mockImplementation((req, res) => res.status(200).json({ id: req.params.id, email: 'updated@example.com' }));

const userRouter = require('../routes/user'); // Replace with actual path to your user routes

app.use('/user', userRouter);

describe('User Routes', () => {
    describe('GET /user', () => {
        it('should get all users if authorized and validated', async () => {
            const response = await request(app).get('/user').set('Authorization', 'Bearer token');

            expect(response.status).toBe(200);
            expect(response.body).toEqual([
                { id: '1', email: 'user1@example.com' },
                { id: '2', email: 'user2@example.com' },
            ]);
            expect(auth.protect).toHaveBeenCalled();
            expect(validateResource).toHaveBeenCalledWith(validateGetAllUsers);
            expect(getAllUsers).toHaveBeenCalled();
        });

        it('should return 401 if not authorized', async () => {
            auth.protect.mockImplementationOnce((req, res, next) => res.status(401).json({ error: 'Unauthorized' }));

            const response = await request(app).get('/user');

            expect(response.status).toBe(401);
            expect(response.body).toEqual({ error: 'Unauthorized' });
        });
    });

    describe('POST /user', () => {
        it('should add a new user if authorized and validated', async () => {
            const response = await request(app).post('/user').set('Authorization', 'Bearer token').send({ email: 'newuser@example.com' });

            expect(response.status).toBe(201);
            expect(response.body).toEqual({ id: '3', email: 'newuser@example.com' });
            expect(auth.protect).toHaveBeenCalled();
            expect(validateResource).toHaveBeenCalledWith(validateAddUser);
            expect(addUser).toHaveBeenCalled();
        });

        it('should return 401 if not authorized', async () => {
            auth.protect.mockImplementationOnce((req, res, next) => res.status(401).json({ error: 'Unauthorized' }));

            const response = await request(app).post('/user').send({ email: 'newuser@example.com' });

            expect(response.status).toBe(401);
            expect(response.body).toEqual({ error: 'Unauthorized' });
        });
    });

    describe('GET /user/:userId', () => {
        it('should get user by ID if authorized and validated', async () => {
            const response = await request(app).get('/user/1').set('Authorization', 'Bearer token');

            expect(response.status).toBe(200);
            expect(response.body).toEqual({ id: '1', email: 'user@example.com' });
            expect(auth.protect).toHaveBeenCalled();
            expect(validateResource).toHaveBeenCalledWith(validateGetUserById);
            expect(getUserById).toHaveBeenCalled();
        });

        it('should return 401 if not authorized', async () => {
            auth.protect.mockImplementationOnce((req, res, next) => res.status(401).json({ error: 'Unauthorized' }));

            const response = await request(app).get('/user/1');

            expect(response.status).toBe(401);
            expect(response.body).toEqual({ error: 'Unauthorized' });
        });
    });

    describe('PUT /user/:id', () => {
        it('should update user if authorized and validated', async () => {
            const response = await request(app).put('/user/1').set('Authorization', 'Bearer token').send({ email: 'updated@example.com' });

            expect(response.status).toBe(200);
            expect(response.body).toEqual({ id: '1', email: 'updated@example.com' });
            expect(auth.protect).toHaveBeenCalled();
            expect(validateResource).toHaveBeenCalledWith(validateUpdateUser);
            expect(updateUser).toHaveBeenCalled();
        });

        it('should return 401 if not authorized', async () => {
            auth.protect.mockImplementationOnce((req, res, next) => res.status(401).json({ error: 'Unauthorized' }));

            const response = await request(app).put('/user/1').send({ email: 'updated@example.com' });

            expect(response.status).toBe(401);
            expect(response.body).toEqual({ error: 'Unauthorized' });
        });
    });
});

