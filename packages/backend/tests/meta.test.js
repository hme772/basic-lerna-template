const request = require('supertest');
const express = require('express');
const auth = require('../middlewares/auth');
const validateResource = require('../middlewares/validateResource');
const { getAllUserTypes } = require('../controls/meta');

const app = express();
app.use(express.json());

jest.mock('../middlewares/auth');
jest.mock('../middlewares/validateResource');
jest.mock('../controls/meta');

// Sample implementations for middleware and controllers to use in tests
auth.protect = jest.fn((req, res, next) => next());
validateResource.mockImplementation((validation) => (req, res, next) => next());

getAllUserTypes.mockImplementation((req, res) => res.status(200).json(['Admin', 'User']));

const metaRouter = require('../routes/meta'); // Replace with actual path to your meta routes

app.use('/meta', metaRouter);

describe('Meta Routes', () => {
    describe('GET /meta', () => {
        it('should get all user types', async () => {
            const response = await request(app).get('/meta');

            expect(response.status).toBe(200);
            expect(response.body).toEqual(['Admin', 'User']);
            expect(getAllUserTypes).toHaveBeenCalled();
        });

    });
});
