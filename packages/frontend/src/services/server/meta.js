import axiosInstance, {routes} from "../axiosConfig";

export const getAllMetaTypes = async () => {
    try {
        return await axiosInstance.get(routes.meta, {});
    } catch (e) {
        return false;
    }
};
