import axiosInstance, {getHeaders, routes} from "../axiosConfig";

export const getAllUsers = async (token) => {
    try {
        return await axiosInstance.get(routes.user, {headers: getHeaders(token)});
    } catch (e) {
        return false;
    }
};

export const getUserById = async ({token, id}) => {
    try {
        if (id.trim() === "") return;
        return await axiosInstance.get(`${routes.user}${id}`, {headers: getHeaders(token)});
    } catch (e) {
        return false;
    }
};

export const getIsUserExist = async ({id, email}) => {
    try {
        if (id.trim() === "" || email.trim() === "") return;
        const result = await axiosInstance.get(`${routes.user}exist/?id=${id}&email=${email}`, {});
        if (result?.data)
            throw new Error(`user already exist! \ncheck entered id and email`);
        return result?.data;
    } catch (e) {
        throw e;
    }
};

export const getUserByEmail = async ({token, email}) => {
    try {
        if (email.trim() === "") return;
        const user = await axiosInstance.get(`${routes.user}?email=${email}`, {headers: getHeaders(token)});
        return user.data[0];
    } catch (e) {
        return false;
    }
};

export const addUser = async ({
                                  token,
                                  id,
                                  firstName,
                                  lastName,
                                  phone,
                                  email,
                                  userType,
                              }) => {
    try {
        if (id.trim() === "") return;
        return await axiosInstance.post(
            routes.user,
            {
                id,
                firstName,
                lastName,
                phone,
                email,
                userType,
            },
            {headers: getHeaders(token)}
        );
    } catch (e) {
        return false;
    }
};

export const registerUserToDb = async ({
                                           token,
                                           id,
                                           firstName,
                                           lastName,
                                           phone,
                                           email,
                                           userType,
                                       }) => {
    try {
        if (id.trim() === "" || email.trim() === "") return;
        return await addUser({
            schema: "template",
            token,
            id,
            firstName,
            lastName,
            phone,
            email,
            userType,
        });
    } catch (e) {
        throw e;
    }
};

export const updateUserInDb = async ({
                                         token, id,
                                         first_name,
                                         last_name,
                                         phone_number,
                                         email,
                                         user_type,
                                         is_active
                                     }) => {
    try {
        if (id?.trim() === "") return;
        if (
            !first_name &&
            !last_name &&
            !phone_number &&
            !email &&
            !user_type &&
            is_active === undefined
        )
            return;
        return await axiosInstance.put(
            `${routes.user}${id}`,
            {
                schema: "template",
                first_name,
                last_name,
                phone_number,
                email,
                user_type,
                is_active
            },
            {headers: getHeaders(token)}
        );
    } catch (e) {
        throw e;
    }
};

export const deleteUserInDb = async ({token, id}) => {
    try {
        if (id.toString().trim() === "") return;
        return await updateUserInDb(
            {
                id: id.toString(),
                token,
                is_active: false
            }
        );
    } catch (e) {
        throw e;
    }
};