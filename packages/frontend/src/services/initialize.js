import {getAllMetaTypes} from "./server/meta";
import {getAllUsers} from "./server/user";
import {slicedStore} from "../store/store";
import {setUsers, setMetaTypes, reset} from '../store/reducers/users-reducer';

export const setInitializedData = async (token) => {
    const metaTypes = await getAllMetaTypes().then(d => d.data);
    slicedStore.dispatch(setMetaTypes(metaTypes));
};

export const setAppData = ({token, id}) => {
    getAllUsers(token).then(users => slicedStore.dispatch(setUsers(users.data)));
};

export const resetAppData = () => {
    slicedStore.dispatch(reset());
};