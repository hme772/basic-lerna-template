import { evnObj } from "./env";
import axios from "axios";

export const routes = {
    user: 'user/',
    meta: 'meta/',
};

export const getHeaders = (token) => {
    if (!token) {
        return undefined;
    }
    return { Authorization: `Bearer ${token}` };
};

const BASE_URL = evnObj.serverUrl;
const axiosInstance = axios.create({
    baseURL:BASE_URL,
    headers:{
        "Content-Type":"application/json"
    }
})

export default axiosInstance;