import {createSelector} from "@reduxjs/toolkit";

const getUsersState = (state) => state.users;
export const getUserTypes = createSelector(
    [getUsersState],
    (users) => {
        const userTypes = [...users?.userTypes];
        return userTypes.sort((a, b) => {
            return a.id - b.id;
        });
    }
);

export const getClients = createSelector(
    [getUsersState],
    (users) => {
        if (!users?.clientsList) return [];
        const clients = [
            ...[...users?.clientsList]?.map((client) => ({
                ...client,
                text: `${client.first_name} ${client.last_name}`,
                value: client.id,
            })),
        ];
        return clients?.sort((a, b) => {
            return a.id - b.id;
        });
    }
);

export const getWorkers = createSelector(
    [getUsersState],
    (users) => {
        const workers = [
            ...users?.workersList?.map((worker) => ({
                ...worker,
                text: `${worker.first_name} ${worker.last_name}`,
                value: worker.id,
            })),
        ];
        return workers?.sort((a, b) => {
            return a.id - b.id;
        });
    }
);

export const getCurrentUser = (id) => createSelector([getUsersState], (users) => {
    const allUsers = [...users?.workersList, ...users?.clientsList];
    if (!id || allUsers.length === 0) return {};
    return allUsers.find(user => user.id == id);
});

export const getModalData = createSelector([getUsersState], (users) => users?.modal);
export const getIsWorkerUser = (userTypeId) => createSelector([getUserTypes], (userTypes) => userTypes
    ?.find((type) => type.id === userTypeId)
    ?.type.trim()
    .toLowerCase() === "worker"
);

export const getWorkersWithoutConnected = (connectedUserId) => createSelector([getWorkers], (workers) => {
    return workers?.filter(worker => worker.value != connectedUserId);
});
export const getClientsWithoutConnected = (connectedUserId) => createSelector([getClients], (clients) => {
    return clients?.filter(client => client.value != connectedUserId);
});