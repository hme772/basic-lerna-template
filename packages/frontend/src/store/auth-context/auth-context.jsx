import React, { useEffect, useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getUserTypes } from "../selectors/users-selector";
import {setMetaTypes} from "../reducers/users-reducer";
import {getAllMetaTypes} from "../../services/server/meta";
import {setAppData, resetAppData} from "../../services/initialize";
import {slicedStore} from "../store";

let logoutTimer;

const AuthContext = React.createContext({
  token: "",
  email: "",
  firstName: "",
  lastName: "",
  id: "",
  isLoggedIn: false,
  login: ({
    token,
    expirationTime,
    email,
    id,
    firstName,
    lastName,
    userType,
  }) => {},
  logout: () => {},
  getUser: () => {},
  getIsWorkerUser: () => {},
});

const calculateRemainingTime = (expirationTimeString) => {
  const currentTime = new Date().getTime();
  const expirationTimeMS = new Date(expirationTimeString).getTime();
  return expirationTimeMS - currentTime;
};

const getStoredToken = () => {
  const storedExpirationDate = localStorage.getItem("expirationTime");
  const remainingTime = calculateRemainingTime(storedExpirationDate);
  const isTokenValid = remainingTime > 3600;
  if (!isTokenValid) {
    localStorage.removeItem("token");
    localStorage.removeItem("expirationTime");
    return null;
  }
  return {
    token: localStorage.getItem("token"),
    duration: remainingTime,
  };
};

const getStoredUser = () => {
  const storedExpirationDate = localStorage.getItem("expirationTime");
  const remainingTime = calculateRemainingTime(storedExpirationDate);
  const isTokenValid = remainingTime > 3600;
  if (!isTokenValid) {
    localStorage.removeItem("id");
    localStorage.removeItem("email");
    localStorage.removeItem("firstName");
    localStorage.removeItem("lastName");
    localStorage.removeItem("userType");
    return null;
  }

  return {
    id: localStorage.getItem("id"),
    email: localStorage.getItem("email"),
    firstName: localStorage.getItem("firstName"),
    lastName: localStorage.getItem("lastName"),
    userType: localStorage.getItem("userType"),
  };
};

export const AuthContextProvider = (props) => {
  const userTypes = useSelector(getUserTypes);
  const dispatch = useDispatch();
  const tokenData = getStoredToken();
  let initialToken;
  if (tokenData) {
    initialToken = tokenData.token;
  }
  const [token, setToken] = useState(initialToken);
  const userData = getStoredUser();
  let initialUser;
  if (userData) {
    initialUser = userData;
    //setAppData({token,id:userData.id});
  }
  const [user, setUser] = useState(initialUser);
  const isUserLoggedIn = Boolean(token); //if there's a token there must be a user

  const onLogout = useCallback(() => {
    setToken(null);
    setUser(null);
    resetAppData();

    localStorage.removeItem("id");
    localStorage.removeItem("email");
    localStorage.removeItem("firstName");
    localStorage.removeItem("lastName");
    localStorage.removeItem("userType");
    localStorage.removeItem("token");
    localStorage.removeItem("expirationTime");
    if (logoutTimer) {
      clearTimeout(logoutTimer);
    }
  }, []);

  const onLogin = ({
    token,
    expirationTime,
    email,
    id,
    firstName,
    lastName,
    userType,
  }) => {
    setToken(token);
    const userTypeString = userTypes?.find((type) => type.id === userType).type;
    setUser({ email, id, firstName, lastName, userType: userTypeString });
    setAppData({token,id});
    localStorage.setItem("token", token);
    localStorage.setItem("expirationTime", expirationTime);
    localStorage.setItem("id", id);
    localStorage.setItem("email", email);
    localStorage.setItem("firstName", firstName);
    localStorage.setItem("lastName", lastName);
    localStorage.setItem("userType", userTypeString);
    const remainingSessionTime = calculateRemainingTime(expirationTime);

    logoutTimer = setTimeout(onLogout, remainingSessionTime);
  };

  const updateUser = ({first_name, last_name, user_type}) => {
    const userTypeString = userTypes?.find((type) => type.id == parseInt(user_type)).type;
    setUser({...user, firstName: first_name, lastName: last_name, userType: userTypeString})
    localStorage.setItem("firstName", first_name);
    localStorage.setItem("lastName", last_name);
    localStorage.setItem("userType", userTypeString);
  };

  const getIsWorkerUser = () =>
    userTypes
      ?.find((type) => type.type === user?.userType)
      ?.type.trim()
      .toLowerCase() === "worker";

  useEffect(() => {
    if (tokenData) {
      console.log("tokenData.duration", tokenData.duration);
      logoutTimer = setTimeout(onLogout, tokenData.duration);
    }
  }, [tokenData, onLogout]);

  useEffect(() => {
    if (
      localStorage.getItem("id") !== null &&
      localStorage.getItem("id")?.trim() !== ""
    ) {
      getAllMetaTypes().then(meta => slicedStore.dispatch(setMetaTypes(meta.data)));
      setAppData({token,id:localStorage.getItem("id")})
    } else {
      resetAppData();
    }
  }, []);
  const contextValue = {
    token,
    email: user?.email,
    firstName: user?.firstName,
    lastName: user?.lastName,
    id: user?.id,
    userType: user?.userType,
    isLoggedIn: isUserLoggedIn,
    login: onLogin,
    logout: onLogout,
    getUser: getStoredUser,
    getIsWorkerUser,
    updateConnectedUser: updateUser,
  };

  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
