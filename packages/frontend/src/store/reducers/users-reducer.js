import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    userTypes: [],
    clientsList: [],
    workersList: [],
    modal: undefined,
};

export const usersSlice = createSlice({
    name: "users",
    initialState,
    reducers: {
        setMetaTypes: (state, action) => {
            state.userTypes = [...action.payload.userTypes];
        },
        setUsers: (state, action) => {
            if (!action.payload) return;
            state.workersList = [...action.payload?.filter((x) => x.user_type === 2)];
            state.clientsList = [...action.payload?.filter((x) => x.user_type === 1)];
        },
        addUser: (state, action) => {
            if (!action.payload) return;
            if (action.payload?.user_type === 2) {
            state.workersList = [...state.workersList, {...action?.payload}];
            }
            else {
                state.clientsList = [...state.clientsList, action.payload];
            }
        },
        updateUser: (state, action) => {
            if (!action.payload) return;
            if (action.payload?.user_type === 2) {
                state.workersList = [...state.workersList.filter((x) => x.id !== action.payload?.id), action.payload];
                state.clientsList = [...state.clientsList.filter((x) => x.id !== action.payload?.id)];
            } else {
                state.workersList = [...state.workersList.filter((x) => x.id !== action.payload?.id)];
                state.clientsList = [...state.clientsList.filter((x) => x.id !== action.payload?.id), action.payload];
            }
        },
        deleteUser: (state, action) => {
            if (!action.payload) return;
            state.workersList = [...state.workersList.filter((x) => x.id !== action.payload)];
            state.clientsList = [...state.clientsList.filter((x) => x.id !== action.payload)];
        },
        setModalData: (state, action) => {
            state.modal = action.payload;
        },
        reset: (state) => {
            state.clientsList = [];
            state.workersList = [];
            state.module = undefined;
        },
    },
});

// Action creators are generated for each case reducer function
export const {
    setUsers,
    addUser,
    updateUser,
    deleteUser,
    setMetaTypes,
    setModalData,
    reset,
} = usersSlice.actions;

export default usersSlice.reducer;
