import { configureStore } from '@reduxjs/toolkit'
import usersReducer from './reducers/users-reducer';

export const slicedStore = configureStore({
    reducer: {
      users: usersReducer
    },
  });
  
export default slicedStore;