import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import store from "./store/store.js";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { AuthContextProvider } from "./store/auth-context/auth-context.jsx";
import { setInitializedData } from "./services/initialize.js";

const initAppData = async () => {
  await setInitializedData();
};
const init = async () => {
  await initAppData();
  ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
      <Provider store={store}>
        <AuthContextProvider>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </AuthContextProvider>
      </Provider>
    </React.StrictMode>
  );
};

init();
