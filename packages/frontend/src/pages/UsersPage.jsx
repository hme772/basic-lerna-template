import UserList from "../components/UserList/UserList";
import {useSelector} from "react-redux";
import {
    getClients,
    getClientsWithoutConnected,
    getWorkers,
    getWorkersWithoutConnected
} from "../store/selectors/users-selector";
import classes from './UsersPage.module.css';

const UsersPage = ({connectedUserId}) => {
    const workersList = useSelector(connectedUserId ? getWorkersWithoutConnected(connectedUserId) : getWorkers);
    const clientsList = useSelector(connectedUserId ? getClientsWithoutConnected(connectedUserId) : getClients);
    return <div className={classes.page}>
        <UserList usersList={workersList} title={"Workers"}/>
        <UserList usersList={clientsList} title={"Clients"}/>
    </div>;
};

export default UsersPage;
