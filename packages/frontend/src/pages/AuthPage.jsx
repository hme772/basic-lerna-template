import AuthForm from '../components/Auth/AuthForm';

const AuthPage = ({registerByWorker=false}) => {
  return <AuthForm registerByWorker={registerByWorker}/>;
};

export default AuthPage;
