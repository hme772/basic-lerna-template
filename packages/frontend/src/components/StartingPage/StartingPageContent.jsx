import classes from './StartingPageContent.module.css';

const StartingPageContent = () => {
  return (
    <section className={classes.starting}>
      <h1>Welcome to Users App!</h1>
    </section>
  );
};

export default StartingPageContent;
