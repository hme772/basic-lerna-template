import {Navigate, Route, Routes} from "react-router-dom";
import HomePage from "../../pages/HomePage";
import AuthPage from "../../pages/AuthPage";
import NewPassword from "../NewPassword/NewPassword";
import {useContext} from "react";
import AuthContext from "../../store/auth-context/auth-context";
import UsersPage from "../../pages/UsersPage";
import Details from "../Details/Details";
import ActionModal from "../ActionModal/ActionModal";


const Router = () => {
    const authCtx = useContext(AuthContext);
    const {isLoggedIn, token, id} = authCtx;

    return <>
        <ActionModal token={token}/>
        <Routes>
        <Route path='/' exact element={<HomePage />}/>
        {!isLoggedIn && <Route path='/auth' element={<AuthPage />}/>}
        <Route path='/new-password' element={isLoggedIn ? <NewPassword /> : <Navigate to='/auth'/>}/>
        <Route path='/details' element={isLoggedIn ? <Details /> : <Navigate to='/auth'/>}/>
        {isLoggedIn && <>
        <Route path='/new-worker' element={<AuthPage registerByWorker={true}/>}/>
        <Route path='/users' element={<UsersPage connectedUserId={id}/>}/>
        </>}
        <Route path='*' element={<Navigate to='/' replace />} />
    </Routes>;
    </>
};

export default Router;