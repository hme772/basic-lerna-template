import {List} from "@mui/material";
import UserItem from "./UserItem";
import classes from './UserList.module.css';

const UserList = ({usersList, title}) => {


    return <List aria-labelledby="decorated-list-demo" sx={{
        margin: '3rem auto',
        width: '100%',
        maxWidth: '25rem',
        borderRadius: '6px',
        backgroundColor: '#38015c',
        boxShadow: '0 1px 4px rgba(0, 0, 0, 0.2)',
        padding: '1rem',
        textAlign: 'center'
    }} className={classes.userList}>
        <h1>{title}:</h1>
        {usersList?.map(user => <UserItem key={user.value} {...user} />)}
    </List>

};

export default UserList;