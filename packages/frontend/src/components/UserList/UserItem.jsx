import {ListItem, ListItemText, Typography, Paper} from "@mui/material";
import Button from "@mui/material/Button";
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import classes from "./UserList.module.css";
import ActionModal from "../ActionModal/ActionModal";
import {useDispatch} from "react-redux";
import {setModalData} from "../../store/reducers/users-reducer";

const UserItem = ({text, value}) => {
    const dispatch = useDispatch();
    return <>
        <ListItem key={value} alignItems="flex-start" sx={{color: "white"}}>
            <Paper elevation={3} style={{
                color: "white",
                backgroundColor: '#9f5ccc',
                borderRadius: '20px',
                padding: '10px',
                width: '100%',
                display: 'flex',
            }}>
                <ListItemText primary={<span style={{fontWeight: 'bold'}}>{text}</span>}/>
                <Button className={classes.customButton} onClick={() => {
                    dispatch(setModalData({id: value, name: text}))
                }}><EditIcon/></Button>
                <Button className={classes.customButton} onClick={() => {
                    dispatch(setModalData({id: value, name: text, isDelete: true}))
                }}><DeleteIcon/></Button>
            </Paper>
        </ListItem>
    </>
};

export default UserItem;