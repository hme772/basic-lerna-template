import { useState, useContext } from "react";
import { Link } from "react-router-dom";
import { Turn as Hamburger } from "hamburger-react";
import {
  Drawer,
  Box,
  Typography,
  List,
  ListItem,
  ListItemText,
} from "@mui/material";
import AuthContext from "../../store/auth-context/auth-context";

import classes from "./MainNavigation.module.css";

const MainNavigation = () => {
  const authCtx = useContext(AuthContext);
  const isLoggedIn = authCtx.isLoggedIn;
  const userData = authCtx.getUser();
  const [isOpen, setOpen] = useState(false);
  const isWorker = authCtx.getIsWorkerUser();
  const handleDrawerToggle = () => {
    setOpen(!isOpen);
  };

  return (
    <header className={classes.header}>
      <Hamburger
        color={"white"}
        direction={"left"}
        toggled={isOpen}
        toggle={handleDrawerToggle}
      />
      <Drawer
        anchor="left"
        open={isOpen}
        onClose={() => setOpen(false)}
        className={classes.drawer}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            width: "200px",
            height: "100%",
            padding: "20px",
            backgroundColor: "#38015c",
            color: "#ffffff",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <Typography
              component="span"
              variant="h6"
              onClick={() => setOpen(false)}
              sx={{ cursor: "pointer" }}
            >
              Menu
            </Typography>
            {isOpen && (
              <Hamburger
                color={"white"}
                direction={"left"}
                toggled={isOpen}
                toggle={handleDrawerToggle}
              />
            )}
          </div>
          <List>
            <ListItem button component={Link} to="/">
              <ListItemText primary="Home" />
            </ListItem>
            {isLoggedIn && (
              <>
                <ListItem button component={Link} to="/new-password">
                  <ListItemText primary="Update Password" />
                </ListItem>
                <ListItem button component={Link} to="/details">
                  <ListItemText primary="Update Details" />
                </ListItem>
                {isWorker && (
                  <>
                    <ListItem button component={Link} to="/new-worker">
                      <ListItemText primary="Register a new worker" />
                    </ListItem>
                    <ListItem button component={Link} to="/users">
                      <ListItemText primary="Show all Users" />
                    </ListItem>
                  </>
                )}
              </>
            )}
          </List>
        </Box>
      </Drawer>
      <Link to="/">
        <div className={classes.logo}>Users</div>
      </Link>
      <nav>
        <ul>
          {!isLoggedIn && (
            <li>
              <Link to="/auth">Login</Link>
            </li>
          )}
          {isLoggedIn && (
            <>
              <li>
                <div>
                  Welcome, {userData?.firstName} {userData?.lastName}
                </div>
              </li>

              <li>
                <button onClick={() => authCtx.logout()}>Logout</button>
              </li>
            </>
          )}
        </ul>
      </nav>
    </header>
  );
};

export default MainNavigation;
