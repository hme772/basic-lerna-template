const path = require('path');
const fs = require('fs');
const { spawn } = require('child_process');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const YAML = require('yaml');

const dockerBaseName = 'template';
const remoteDockerBaseRepo = 'some amazon key.some zone.amazonaws.com';
const remoteDockerRegion = 'some zone';

const getVersion = (packageJsonPath) => {
  const content = fs.readFileSync(packageJsonPath);
  const data = JSON.parse(content);
  return data.version;
};

const runCommand = (commandArgs, logOutput = true, logError = true) => {
  return new Promise((resolve, reject) => {
    const process = spawn(commandArgs[0], commandArgs.slice(1));
    let output = '';
    let error = '';
    process.stdout.setEncoding('utf8');
    process.stderr.setEncoding('utf8');
    process.stdout.on('data', (data) => {
      output += data;
      if (logOutput) {
        console.log(data);
      }
    });
    process.stderr.on('data', (data) => {
      error += data;
      if (logOutput) {
        console.error(data);
      }
    });
    process.on('exit', (code) => {
      const result = { code, output, error };
      if (code === 0) {
        resolve(result);
      } else {
        reject(result);
      }
    });
  });
};

const buildDocker = async (serviceName, directoryPath) => {
  console.log(`Building ${serviceName} docker...`);
  const dockerfile = path.join(directoryPath, 'Dockerfile');
  const version = getVersion(path.join(directoryPath, 'package.json'));
  const commandArgs = [
    'docker',
    'build',
    '-t',
    `${dockerBaseName}/${serviceName}:${version}`,
    '-f',
    `${dockerfile}`,
    '.',
  ];
  await runCommand(commandArgs);
  await tagDocker(serviceName, version, dockerBaseName, 'latest');
};

const tagDocker = (
  serviceName,
  fromVersion,
  toBase = undefined,
  toVersion = 'latest'
) => {
  const currentTag = `${dockerBaseName}/${serviceName}:${fromVersion}`;
  const newTag = `${toBase ?? dockerBaseName}/${serviceName}:${toVersion}`;
  console.log(`Tagging ${serviceName} (${currentTag}) docker to ${newTag}...`);
  const commandArgs = ['docker', 'tag', currentTag, newTag];
  return runCommand(commandArgs);
};

const loginDocker = async () => {
  let password = '';
  const getPasswordCommandArgs = [
    'aws',
    'ecr',
    'get-login-password',
    '--region',
    remoteDockerRegion,
  ];
  try {
    const { output } = await runCommand(getPasswordCommandArgs, false);
    password = output;
  } catch (err) {
    console.error('Failed to login to remote repo', err);
    return;
  }

  const loginCommandArgs = [
    'docker',
    'login',
    '--username',
    'AWS',
    '--password',
    password,
    'some amazon key.some zone.amazonaws.com',
  ];
  await runCommand(loginCommandArgs);
};

const pushDocker = async (serviceName, directoryPath) => {
  const version = getVersion(path.join(directoryPath, 'package.json'));
  const remoteDockerBase = `${remoteDockerBaseRepo}/${dockerBaseName}`;
  await tagDocker(serviceName, version, remoteDockerBase, version);
  await tagDocker(serviceName, 'latest', remoteDockerBase, 'latest');
  await loginDocker();
  console.log('Pushing to remote repo...');
  const pushCommandArgs = [
    'docker',
    'push',
    `${remoteDockerBase}/${serviceName}:${version}`,
  ];
  await runCommand(pushCommandArgs);
  pushCommandArgs[2] = `${remoteDockerBase}/${serviceName}:latest`;
  await runCommand(pushCommandArgs);
};

const pullDocker = async (serviceName) => {
  const content = fs.readFileSync('docker-compose-backend.yaml', {
    encoding: 'utf-8',
  });
  const dockerCompose = YAML.parse(content);
  const image = dockerCompose.services[serviceName].image;
  await loginDocker();
  const pullCommandArgs = ['docker', 'pull', image];
  await runCommand(pullCommandArgs);
};

const serviceToPathMap = {
  backend: './packages/backend',
  frontend: './packages/frontend',
};

const runCommands = async (argv) => {
  for (const serviceName of argv.target) {
    const servicePath = serviceToPathMap[serviceName];
    if (argv.build) {
      await buildDocker(serviceName, servicePath);
    }

    if (argv.push) {
      await pushDocker(serviceName, servicePath);
    }

    if (argv.pull) {
      await pullDocker(serviceName);
    }
  }
};

const argv = yargs(hideBin(process.argv))
  .option('build', {
    alias: 'b',
  })
  .option('push', {
    alias: 'p',
  })
  .option('pull')
  .option('target', {
    alias: 't',
    type: 'string',
    array: true,
  })
  .parse();

runCommands(argv);
